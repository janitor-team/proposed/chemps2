CMAKE_MINIMUM_REQUIRED(VERSION 3.0.2)

# Same as for debian/rules and/or source

set(HDF5_INCLUDE_DIRS /usr/include/hdf5/serial)

set(CHEMPS2_INCLUDE_DIRS /usr/include/chemps2)

# Build

SET(TARGET "upstream")

PROJECT(${TARGET})

add_executable(${TARGET} ${TARGET}.cpp)

target_include_directories (${TARGET} PRIVATE ${CHEMPS2_INCLUDE_DIRS} ${HDF5_INCLUDE_DIRS})

target_link_libraries (${TARGET} chemps2)

enable_testing()

add_test(${TARGET} ${TARGET})

